const express = require('express');
const app = express();
const fs = require('fs');
const listNameOfFiles = './files/';
const path = require('path');
const morgan = require('morgan');
const arrOfExtension = ["log", "txt", "json", "yaml", "xml", "js", ".log", ".txt", ".json", ".yaml", ".xml", ".js"]

app.use(express.json());

const logFile = fs.createWriteStream('./requests.log', { flags: 'a' });
app.use(morgan('tiny', { stream: logFile }));


app.get('/api/files', (req, res) => { 
    return res.send({
        "message": "Success",
        "files": readNameOfFiles()
    });
});

app.get('/api/files/:filename', function(req, res) {

    const allFiles = readNameOfFiles();
    if (!allFiles.includes(req.params.filename)){
        res.status(400).send({
            "message": `No file with '${req.params.filename}' filename found`
        })
    } else {
        const contents = fs.readFileSync(`${listNameOfFiles}${req.params.filename}`, 'utf8');
        const arr = req.params.filename.split('.')
        const extension = arr[arr.length - 1];
        const stats = fs.statSync(`${listNameOfFiles}${req.params.filename}`);
    
        res.send({
            "message": "Success",
            "filename": req.params.filename,
            "content": contents,
            "extension": extension,
            "uploadedDate": stats.birthtime
        });
        }
    
    
});
app.post('/api/files', (req, res) => {

    
        if (!req.body.filename || !req.body.content) {
            const missedField = !req.body.filename ? 'filename' : 'content'
            res.status(400).send({
                'message': `Please specify '${missedField}' parameter`
            })
        } else {
            const checkOFExtension = path.extname(req.body.filename);
            if(arrOfExtension.includes(checkOFExtension)){
                createFile(req.body.filename, req.body.content)
                return res.send({
                    "message": "File created successfully"
                });
            } else {
                return res.status(400).send({
                    "message": "Invalid extension"
                })        
            }
        }
});

app.delete('/api/files/:filename', (req, res) => {

    const allFiles = readNameOfFiles();
    if (!allFiles.includes(req.params.filename)){
        res.status(400).send({
            "message": `No file with '${req.params.filename}' filename found`
        })
    } else {
        fileDelete(req.params.filename);
        res.send({
            "message": 'File deleted successfully!'
        })
    } 
})

app.put('/api/files', (req, res) => {

    const allFiles = readNameOfFiles();
    if (!allFiles.includes(req.body.filename)){
        res.status(400).send({
            "message": `No file with '${req.body.filename}' filename found`
        })
    } else {
        createFile(req.body.filename, req.body.content)
        res.send({
            "message": "Content updated"
        })
    }
})

app.listen(8080, function() {
  console.log('Example app listening on port 8080!');
});


const fileDelete = (filename) => {
    fs.unlink(`${listNameOfFiles}${filename}`, (err) => {
        if(err) throw err;
        console.log('File deleted successfully!');
    });

}
const initDirectory = () => {
    const dir = './files';

    if (!fs.existsSync(dir)){
        fs.mkdirSync(dir);
    }
}
initDirectory();

const readNameOfFiles = () => {
    return fs.readdirSync(listNameOfFiles, {withFileTypes: true})
        .map(item => item.name)
    }

const createFile = (fileName, content) => {

    return fs.appendFile(`${listNameOfFiles}${fileName}`, content, function (err) {
            if (err) throw err;
            console.log(fileName, content, 'Saved!');
        });
}

app.get('*', function(req, res){
    res.status(500).send({
        "message": "Server error"
    });
});
app.post('*', function(req, res){
    res.status(500).send({
        "message": "Server error"
    });
});
app.delete('*', function(req, res){
    res.status(500).send({
        "message": "Server error"
    });
});
app.put('*', function(req, res){
    res.status(500).send({
        "message": "Server error"
    });
});